# Pr�ctica 7 - PDL - Analizador de PL0 ampliado utilizando Jison

## Autores

* Cristo Gonz�lez Rodr�guez - alu0100694987
* Sawan J. Kapai Harpalani - alu0100694765

## Objetivo de la Pr�ctica

El objetivo de la pr�ctica reside en la utilizaci�n de Jison, un generador autom�tico de analizadores a partir de una gram�tica, para la creaci�n de un analizador de PL0 ampliado.
Se debe especificar la gram�tica reconocida, en el correspondiente apartado, asi como permitir guardar un m�ximo prefijado de programas, suprimiendo uno al azar en caso de superar el l�mite.
Permitiendo �nicamente almacenar ficheros a los usuarios autentificados (para lo que se debe extender la autentificaci�n OAuth), con Google y otra plataforma que proporcione dicho servicio.
Por �ltimo se realizaran una serie de pruebas que permitir�n comprobar el correcto funcionamiento de todas las posibles sentencias del lenguaje que comprende el analizador; asi como una serie de situaciones de error.

## Recursos utilizados

* Jison: http://zaach.github.io/jison/ - Generaci�n del analizador ampliado de PL0
* OAuth: http://oauth.net/ - Protocolo abierto que permite la autentificaci�n segura
* Heroku: https://www.heroku.com/ - Plataforma de aplicaciones en la nube
* Heroku Postgres: https://devcenter.heroku.com/articles/heroku-postgresql - Servicio de base de datos SQL proporcionado por Heroku
* DataMapper: http://datamapper.org/ - ORM escrito en Ruby

## Realizaci�n de la Pr�ctica

* README.md - Cristo [OK]
* P�gina principal y vistas - Cristo | Sawan [OK]
* Documentaci�n de la gram�tica - Cristo [OK]
* Implementaci�n Analizador - Cristo [OK]
* Code Mirror -Cristo [OK]
* Base de Datos - Cristo | Sawan [OK]
* Autentificaci�n - Sawan [OK]
* Pruebas - Sawan [OK]
* Despliegue Heroku - Sawan [OK]
* Estilo - Cristo | Sawan [OK]

## Despliegue

* Heroku: http://analizador-p7pdl.herokuapp.com/
* Pruebas: http://analizador-p7pdl.herokuapp.com/tests